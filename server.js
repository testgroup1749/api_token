//Import le packet_express
const express = require('express');

// cree une application express
const app = express();
//on utilisera des json pour le passage et la récupération des paramétres
app.use(express.json);
//et pour encoder le contenu
app.use(express.urlencoded({extended: true}));


//Ecoute la méthode GET de la route '/api/hello'
app.get('/api/hello', (req, res)=>{
    res.send('Hello World!')
})


//La route /api/login
//charger la lib jsonwebtoken
const jwt = require('jsonwebtoken');


//Charger fichier env qui contient les clé secrets
require('dotenv').config();


//charger le fichier env pour le mettre dans le process
process.env.ACCESS_TOKEN_SECRET;
//console.log('secret key is',process.env.ACCESS_TOKEN_SECRET);


//déclarere un user pas de base de donnée
const user = {
    login: 'admin',
    password: 'admin'
};


//fonction qui génerer access token
//user : la data qu'on veut encrypté + clé secret + durée expiration
function generateAccessToken(user){
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {expiresIn: '1800s'});

}


//Ecoute la méthode POST de la route '/api/login'
app.post('/api/login', (req, res)=>{
    if(req.body.login != user.login){
        res.status(401).send("invalid credentials!");
        return;/*arreter le process*/
    }
    if(req.body.password != user.password){
        res.status(401).send("invalid credentials!");
        return;/*arreter le process*/
    }
    //generer notre premier jwt
    //on génere access token qui est indéchiffrable sans la clé secret qui est dan .env

    const accessToken = generateAccessToken(user);
    //envoyé l'access token
    res.send({
        'token' : accessToken,
    });
    
});


//Ecoute la méthode POST de la route '/api/secret'
app.post('/api/secret', (req, res)=>{
    if(req.body.api_token != accessToken){
        res.status(401).send("invalid token!");
        return;/*arreter le process*/
    }
    res.status(200).send("This is a super secret information");
    
});

//PORT
const PORT = 8088

//Démarer le serveur
app.listen(PORT, ()=>{
    console.log('Serveur démarré : http://localhost:8088')
})


